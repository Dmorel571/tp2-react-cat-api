import React from 'react';
import './App.css';
import ListeChat from './ListeChat';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chats: [],
    };
  }
  componentDidMount() {
    fetch('https://api.thecatapi.com/v1/images/search', {
      headers: {
        'x-api-key': '16abd6e6-5b4e-4629-8841-747419be5a6b'
      }
    })
  .then((res) => res.json())
  .then((data) => {
    this.setState({ chats: data.message })
  })
}


  render() {
    return (
      <div>
        <h1 style={{textAlign: 'center'}}>
          Voici des chats!
        </h1>
        <ListeChat chats={this.state.chats}/>
      </div>
    );
  }
}

export default App;
