import React from "react";
import Chat from './Chat';

const ListeChat = (props) => {
    
    const chatsArray = props.chats.map((chatURL)=> {
        return <Chat url={chatURL}/>
    })
    return (
        <div className="container">
            {chatsArray}
        </div>
    )
}


export default ListeChat;